<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/login', 'AuthenController@index');
Route::get('/register', 'AuthenController@register');
Route::post('/register', 'AuthenController@PostRegister');
Route::post('/login', 'AuthenController@PostLogin');