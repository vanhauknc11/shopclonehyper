<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\Categories;

class HomeController extends Controller
{
    public function index(){

       
        $site = $this->getConfig();
        $lang = $this->getLang();

        $result = $this->showCategory();

        return view('index',
        ['site'=>$site,
        'lang'=>$lang,
        'results'=>$result->toArray()
        ]);
    }

    public function getConfig(){
       $data =  DB::table('setting')->select('*')->get();
       $myArray = json_decode(json_encode($data[0]), true);
       return $myArray;
    
    }
    public function getLang(){
        $data =  DB::table('lang')->select('*')->get();
        $myArray = json_decode(json_encode($data[0]), true);
        return $myArray;
     
     }

     public function showCategory(){
        $result = "";
        $result = Categories::where('display','show')->get();
        return $result;
     }

}
