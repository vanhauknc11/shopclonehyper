<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Services\Service;
use Illuminate\Support\Facades\Session;

class AuthenController extends Controller
{

    protected $service;

    public function __construct(Service $service ){
        $this->service = $service;
    }

    public function index(){
        $site = $this->getConfig();
        return view('Authen.Login',
        [
            'site'=>$site,
        ]);
    }

    public function register(){
        $site = $this->getConfig();
        return view('Authen.Register',[
            'site'=>$site,
        ]);
    }

    public function PostRegister(Request $request){
         $res =  $request->all() ;
        
         
         
         if($res['btnDangKy']){
            $username =$this->service->check_string($res['username']);
            $email =$this->service->check_string($res['email']);
            $password =$this->service->check_string($res['password']);
            $password =md5($password);
            Session::flash('message','<script type="text/javascript">swal("Thất Bại", "Vui lòng nhập Username hợp lệ", "error");</script>');
            return redirect()->back() ;
         }
    }

    public function PostLogin(Request $request){

    }

    public function getConfig(){
        $data =  DB::table('setting')->select('*')->get();
        $myArray = json_decode(json_encode($data[0]), true);
        return $myArray;
     
     }
}
